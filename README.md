# PSI-Projetos-2023

Esta é uma API Restful que conta com as CRUDS básicas (index, show, create, update e delete), feita para o trabalho de Tech Lead do PSI de projetos da EJCM.
Aqui você deve por uma breve descrição sobre o projeto e sua finalidade.
 
**Status do Projeto** : Terminado 

![JavaScript](https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E)
![Node Js](https://img.shields.io/badge/Node.js-339933?style=for-the-badge&logo=nodedotjs&logoColor=white)
![Sequelize](https://img.shields.io/badge/Sequelize-52B0E7?style=for-the-badge&logo=Sequelize&logoColor=white)

 
## Tecnologias utilizadas

Essas são as frameworks e ferramentas que você precisará instalar para desenvolver esse projeto:

 - Node Js
 - Sequelize
 - Express

Outras tecnologias interessantes que foram implementadas:
 - jasonwebtoken


## Instalação 
* Rode no terminal o comando seguinte para clonar o repositório. *

``` bash
$ git clone https://gitlab.com/Francisco_acs/psi-projetos-2023.git
```

## Configuração

*Para configurar e realizar as instalações necessárias*


``` bash
$ cd psi-projeto-2023/back
```
``` bash
$ npm i
```
 
## Uso

*Para rodar o projeto e testar as cruds.*


``` bash
$ npm run migrate
```
``` bash
$ npm run dev
```
``` bash
$ Digite seu comando aqui
```


## Autores
 
* Dev Back-end - Francisco Augusto  
