
const Express = require('express');
const router = Express();
const UserController = require('../controllers/UserController');
const CarController = require('../controllers/CarController');
const AuthController = require('../controllers/AuthController');

//rotas para user
router.post('/user',UserController.create);
router.get('/user/:id',UserController.show);
router.get('/user/',UserController.index);
router.put('/user/:id',UserController.update);
router.delete('/user/:id',UserController.destroy);

//rotas para car
router.post('/car',CarController.create);
router.get('/car/:id',CarController.show);
router.get('/car/',CarController.index);
router.put('/car/:id',CarController.update);
router.delete('/car/:id',CarController.destroy);

//rotas para relação
router.put('/user/:userId/car/:carId', CarController.addUser);
router.delete('/car/:id',CarController.removeUser);

//rotas para autenticação
const passport = require("passport");
router.use("/private", passport.authenticate('jwt', {session: false}));
router.post("/login", AuthController.login);
router.get("/private/getDetails", AuthController.getDetails);


module.exports = router;