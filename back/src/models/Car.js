const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Car = sequelize.define('Car', {
    brand: {
        type: DataTypes.STRING,
        allowNull: false
    },
    model: {
        type: DataTypes.STRING,
        allowNull: false
    },
    color: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

Car.associate = function (models) {
    Car.belongsTo(models.User);
}

module.exports = Car;