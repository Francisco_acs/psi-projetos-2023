const crypto = require("crypto");
const fs = require("fs");
const path = require("path");

(()=>{
	const keyPair = crypto.generateKeyPairSync('rsa', {
		modulusLength: 2048,

        publicKeyEncoding: {
            type: 'spki',
            format: 'pem',
        },
        privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem',
        },
	});

	fs.writeFileSync(path.join(__dirname, "..", "..","id_rsa_pub.pem"), keyPair.publicKey);
	fs.writeFileSync(path.join(__dirname, "..", "..","id_rsa_priv.pem"), keyPair.privateKey);

	console.log("As chaves foram geradas!!!")
})();
