const Car = require('../models/Car');
const User = require('../models/User');

const create = async (req, res) => {
    try{
        const car = await Car.create(req.body);
        return res.status(201).json({message: "Carro cadastrado!", Car: car});
    }catch(e){
        return res.status(500).json({e});
    }
};

const index = async (req, res) => {
    try {
        const cars = await Car.findAll();
        return res.status(200).json({cars})
    }catch(e){
        return res.status(500).json({e})
    }
};

const show = async (req, res) => {
    const { id } = req.params;
    try {
        const car = await Car.findByPk(id);
        return res.status(200).json({car})
    }catch(e){
        return res.status(500).json({e});
    }
};

const update =  async (req, res) => {
    const { id } = req.params;

    try {
        const [updated] =  await Car.update(req.body, {where: {id: id}});
        if(updated) {
            const car = await Car.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    }catch(e){
        return res.status(500).json("Carro não encontrado")
    }
};

const destroy = async (req, res) => {
    const { id } = req.params;
    try{
        const deleted = await Car.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Carro deletado com sucesso");            
        }
        throw new Error();
    }catch(e){
        return res.status(500).json("Usuário não encontrado.");
    }
};

const addUser = async (req, res) => {
    const {userId, carId} = req.params;
    try{
        const user = await User.findByPk(userId);
        const car = await Car.findByPk(carId);
        await car.setUser(user);
        return res.status(200).json(car);
    }catch(e){
        return res.status(500).json({e});
    }
}

const removeUser = async (req, res) => {
    const { Id } = req.params;
    try{
        const car = await Car.findByPk(Id);
        await car.setUser(null);
        return res.status(200).json(car);
    }catch(e){
        return res.status(500).json({e});
    }
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addUser,
    removeUser
};
