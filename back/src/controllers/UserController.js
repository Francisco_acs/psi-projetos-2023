const User = require('../models/User');
const bcrypt = require('bcryptjs');
const auth = require('../config/auth');

const create = async(req,res) => {
    try{
          const { password } = req.body;
          const HashSalt = auth.generatePassword(password);
          const salt = HashSalt.salt;
          const hash = HashSalt.hash;
          const data = {
            name: req.body.name,
            email: req.body.email,
            hash: hash,
            salt: salt
          };
          const user = await User.create(data);
          return res.status(201).json({message: "Usuário cadastrado com sucesso!", user: user});
      }catch(err){
          res.status(500).json({error: err});
      }
};


const index = async (req, res) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({users})
    }catch(e){
        return res.status(500).json({e})
    }
};

const show = async (req, res) => {
    const { id } = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({user})
    }catch(e){
        return res.status(500).json({e});
    }
};

const update =  async (req, res) => {
    const { id } = req.params;

    try {
        const [updated] =  await User.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error();
    }catch(e){
        return res.status(500).json("Usuário não encontrado")
    }
};

const destroy = async (req, res) => {
    const { id } = req.params;
    try{
        const deleted = await User.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso");            
        }
        throw new Error();
    }catch(e){
        return res.status(500).json("Usuário não encontrado.");
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy
};

